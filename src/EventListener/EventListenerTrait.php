<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventListener;

use RvaVzw\KrakBoem\Event\Event;

trait EventListenerTrait
{
    public function __invoke(Event $event): void
    {
        $reflection = new \ReflectionClass($this);

        foreach ($reflection->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            $attributes = $method->getAttributes(After::class);
            foreach ($attributes as $attribute) {
                if ($event instanceof ($attribute->newInstance()->eventClass)) {
                    $method->invoke($this, $event);
                }
            }
        }
    }
}
