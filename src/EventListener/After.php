<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventListener;

use RvaVzw\KrakBoem\Event\Event;

#[\Attribute(\Attribute::TARGET_METHOD)]
final readonly class After
{
    /**
     * @param class-string<Event> $eventClass
     */
    public function __construct(
        public string $eventClass,
    ) {
    }
}
