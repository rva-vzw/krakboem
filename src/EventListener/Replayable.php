<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventListener;

use RvaVzw\KrakBoem\Event\Event;

interface Replayable extends EventListener
{
    public function __invoke(Event $event): void;
}
