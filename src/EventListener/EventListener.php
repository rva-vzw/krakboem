<?php

namespace RvaVzw\KrakBoem\EventListener;

use RvaVzw\KrakBoem\Event\Event;

interface EventListener
{
    public function __invoke(Event $event): void;
}
