<?php

namespace RvaVzw\KrakBoem\Command;

use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

interface Command
{
    public function getAggregateRootIdentifier(): AggregateRootIdentifier;
}
