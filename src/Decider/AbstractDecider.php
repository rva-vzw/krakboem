<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Decider;

use RvaVzw\KrakBoem\Command\Command;
use RvaVzw\KrakBoem\Event\Event;

/**
 * @template InternalState of AggregateInternalState
 *
 * @implements Decider<InternalState>
 */
abstract readonly class AbstractDecider implements Decider
{
    public function decide(AggregateInternalState $internalState, Command $command): iterable
    {
        $reflection = new \ReflectionClass($this);

        foreach ($reflection->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            $attributes = $method->getAttributes(Handles::class);
            foreach ($attributes as $attribute) {
                if ($command instanceof ($attribute->newInstance()->commandClass)) {
                    /** @var iterable<Event> $events */
                    $events = $method->invoke($this, $internalState, $command);
                    yield from $events;
                }
            }
        }

        // If not handled, return no events.
        yield from [];
    }

    public function evolve(AggregateInternalState $internalState, Event $event): AggregateInternalState
    {
        $reflection = new \ReflectionClass($this);

        foreach ($reflection->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            $attributes = $method->getAttributes(Applies::class);
            foreach ($attributes as $attribute) {
                if ($event instanceof ($attribute->newInstance()->eventClass)) {
                    /** @var InternalState $internalState */
                    $internalState = $method->invoke($this, $internalState, $event);

                    return $internalState;
                }
            }
        }

        // If nothing needed to be applied:
        return $internalState;
    }
}
