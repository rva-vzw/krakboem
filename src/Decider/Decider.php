<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Decider;

use RvaVzw\KrakBoem\Command\Command;
use RvaVzw\KrakBoem\Event\Event;

/**
 * @template InternalState of AggregateInternalState
 */
interface Decider
{
    /**
     * @param InternalState $internalState
     *
     * @return iterable<Event>
     */
    public function decide(
        AggregateInternalState $internalState,
        Command $command,
    ): iterable;

    /**
     * @param InternalState $internalState
     *
     * @return InternalState
     */
    public function evolve(
        AggregateInternalState $internalState,
        Event $event,
    ): AggregateInternalState;

    /**
     * @return InternalState
     */
    public function getInitialState(): AggregateInternalState;

    /**
     * @param InternalState $internalState
     */
    public function isTerminal(AggregateInternalState $internalState): bool;
}
