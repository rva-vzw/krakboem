<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Decider;

use RvaVzw\KrakBoem\Command\Command;

#[\Attribute(\Attribute::TARGET_METHOD)]
final readonly class Handles
{
    /**
     * @param class-string<Command> $commandClass
     */
    public function __construct(
        public string $commandClass,
    ) {
    }
}
