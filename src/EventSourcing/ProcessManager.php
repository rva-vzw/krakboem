<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing;

use RvaVzw\KrakBoem\EventListener\EventListener;

interface ProcessManager extends EventListener
{
}
