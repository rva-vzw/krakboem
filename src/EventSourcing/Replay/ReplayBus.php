<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing\Replay;

use RvaVzw\KrakBoem\Event\Event;

interface ReplayBus
{
    public function publish(Event $event): void;
}
