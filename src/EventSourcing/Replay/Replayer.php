<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing\Replay;

use Psr\Log\LoggerInterface;
use RvaVzw\KrakBoem\EventStore\EventStore;

final readonly class Replayer
{
    public function __construct(
        private readonly ReplayBus $bus,
        private readonly EventStore $eventStore,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function replayAll(): void
    {
        foreach ($this->eventStore->getStream() as $event) {
            try {
                $this->bus->publish($event);
            } catch (\Throwable $e) {
                $eventClass = get_class($event);
                $this->logger->error(
                    'Exception during replay while handling {class} event: {message}', [
                        'class' => $eventClass,
                        'message' => $e->getMessage(),
                        'exception' => $e,
                    ],
                );
            }
        }
    }
}
