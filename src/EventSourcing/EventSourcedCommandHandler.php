<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing;

use RvaVzw\KrakBoem\Command\Command;
use RvaVzw\KrakBoem\CommandHandler\CommandHandler;
use RvaVzw\KrakBoem\Decider\AggregateInternalState;
use RvaVzw\KrakBoem\Decider\Decider;
use RvaVzw\KrakBoem\EventBus\EventBus;
use RvaVzw\KrakBoem\EventStore\EventStore;

/**
 * @template InternalState of AggregateInternalState
 */
abstract readonly class EventSourcedCommandHandler implements CommandHandler
{
    /**
     * @param Decider<InternalState> $decider
     */
    public function __construct(
        private EventStore $eventStore,
        private EventBus $eventBus,
        private Decider $decider,
    ) {
    }

    /**
     * Return true if the command applies to the aggregate.
     */
    abstract protected function acceptsCommand(Command $command): bool;

    /**
     * @param Command $command
     */
    final public function __invoke($command): void
    {
        if (!$this->acceptsCommand($command)) {
            return;
        }

        $stream = $this->eventStore->getStreamForAggregate(
            $command->getAggregateRootIdentifier(),
        );

        $internalState = $this->decider->getInitialState();

        // The playhead count is rather lousy, but I guess it will do
        $playhead = 0;
        foreach ($stream as $event) {
            ++$playhead;
            $internalState = $this->decider->evolve($internalState, $event);
        }

        foreach ($this->decider->decide($internalState, $command) as $event) {
            $this->eventBus->publish($event, intval(++$playhead));
        }
    }
}
