<?php

namespace RvaVzw\KrakBoem\EventStore;

use RvaVzw\KrakBoem\Event\Event;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

interface EventStore
{
    public function store(Event $event, int $playhead): void;

    /**
     * @return \iterator<Event>
     */
    public function getStreamForAggregate(AggregateRootIdentifier $aggregateRootIdentifier): \Traversable;

    public function hasStreamForAggregate(AggregateRootIdentifier $identifier): bool;

    /**
     * @return \iterator<Event>
     */
    public function getStream(): \Traversable;
}
