<?php

namespace RvaVzw\KrakBoem\CommandHandler;

use RvaVzw\KrakBoem\Command\Command;

interface CommandHandler
{
    public function __invoke(Command $command): void;
}
