<?php

namespace RvaVzw\KrakBoem\CommandBus;

use RvaVzw\KrakBoem\Command\Command;

interface CommandBus
{
    public function dispatch(Command ...$commands): void;
}
