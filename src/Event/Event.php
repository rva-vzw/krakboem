<?php

namespace RvaVzw\KrakBoem\Event;

use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

/**
 * A domain event.
 */
interface Event
{
    public function getAggregateRootIdentifier(): AggregateRootIdentifier;
}
