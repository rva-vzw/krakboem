<?php

namespace RvaVzw\KrakBoem\Id;

/**
 * Marker interface for objects that should be (de)normalized from and to an int.
 */
interface IntWrapper
{
    /**
     * @return static
     */
    public static function fromInteger(int $value): self;

    public function toInteger(): int;
}
