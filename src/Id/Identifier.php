<?php

namespace RvaVzw\KrakBoem\Id;

/**
 * Some identifier.
 */
interface Identifier
{
    /**
     * @param non-empty-string $value
     *
     * @return static
     */
    public static function fromString(string $value): self;

    /**
     * @return non-empty-string
     */
    public function toString(): string;
}
