<?php

namespace RvaVzw\KrakBoem\Infrastructure\Uuid;

use Ramsey\Uuid\Rfc4122\FieldsInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use RvaVzw\KrakBoem\Id\Identifier;

abstract readonly class Uuid5Identifier implements Identifier
{
    /** @var non-empty-string */
    private string $value;

    final private function __construct(UuidInterface $uuid)
    {
        $this->value = $uuid->toString();

        /** @var FieldsInterface $fields */
        $fields = $uuid->getFields();

        if (5 == $fields->getVersion()) {
            return;
        }

        throw new \InvalidArgumentException('Uuid v5 expected.');
    }

    abstract protected static function getNamespace(): string;

    /**
     * @return static
     */
    public static function fromString(string $value): Identifier
    {
        // Go via Uuid to make sure that we're dealing with a UUID.
        return new static(Uuid::fromString($value));
    }

    /**
     * @return non-empty-string
     */
    public function toString(): string
    {
        return $this->value;
    }

    /**
     * @return static
     */
    public static function fromName(string $name): self
    {
        $uuid = Uuid::uuid5(static::getNamespace(), $name);

        /* @noinspection PhpUnhandledExceptionInspection */
        return new static($uuid);
    }
}
