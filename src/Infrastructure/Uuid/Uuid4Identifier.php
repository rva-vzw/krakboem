<?php

namespace RvaVzw\KrakBoem\Infrastructure\Uuid;

use Ramsey\Uuid\Uuid;
use RvaVzw\KrakBoem\Id\Identifier;

abstract readonly class Uuid4Identifier implements Identifier
{
    /** @var non-empty-string */
    private string $value;

    /**
     * Public constructor.
     *
     * I'm not sure the constructor should actually be public.
     *
     * @param non-empty-string $value
     */
    final public function __construct(string $value)
    {
        // Check whether this is a valid UUID.
        if (!Uuid::isValid($value)) {
            throw new \InvalidArgumentException('Invalid UUID');
        }
        $this->value = strtolower($value);
    }

    /**
     * @param non-empty-string $value
     *
     * @return static
     */
    public static function fromString(string $value): Identifier
    {
        // Go via Uuid to make sure that we're dealing with a UUID.
        return new static($value);
    }

    /**
     * @return static
     */
    public static function create(): self
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $uuid4 = Uuid::uuid4();

        return new static($uuid4->toString());
    }

    /**
     * @return non-empty-string
     */
    public function toString(): string
    {
        return $this->value;
    }
}
