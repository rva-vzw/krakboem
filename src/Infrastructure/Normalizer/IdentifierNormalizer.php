<?php

namespace RvaVzw\KrakBoem\Infrastructure\Normalizer;

use RvaVzw\KrakBoem\Id\Identifier;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final readonly class IdentifierNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param array<mixed>|false $context
     */
    public function normalize(mixed $object, ?string $format = null, array|false $context = []): string
    {
        Assert::isInstanceOf($object, Identifier::class);

        return $object->toString();
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed  $data   Data to normalize
     * @param array<mixed> $context
     */
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof Identifier;
    }

    /**
     * @param array<mixed> $context
     *
     * @return Identifier
     */
    public function denormalize(mixed $data, string $type, ?string $format = null, array $context = []): mixed
    {
        if (!class_exists($type)) {
            throw new \InvalidArgumentException();
        }

        $reflectionMethod = new \ReflectionMethod($type, 'fromString');

        /** @var Identifier $identifier */
        $identifier = $reflectionMethod->invoke(null, $data);

        return $identifier;
    }

    /**
     * Checks whether the given class is supported for denormalization by this normalizer.
     *
     * @param mixed  $data   Data to denormalize from
     * @param class-string $type   The class to which the data should be denormalized
     * @param ?string $format The format being deserialized from
     * @param array<mixed> $context
     */
    public function supportsDenormalization(mixed $data, string $type, ?string $format = null, array $context = []): bool
    {
        if ('[]' === substr($type, -2, 2)) {
            return false;
        }

        $interfaces = class_implements($type);

        return in_array(Identifier::class, $interfaces);
    }

    /**
     * @return array<class-string, bool>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [Identifier::class => true];
    }
}
