<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Infrastructure\Normalizer;

use RvaVzw\KrakBoem\Id\Identifier;
use RvaVzw\KrakBoem\Id\IntWrapper;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final readonly class IntWrapperNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param array<mixed> $context
     *
     * @return Identifier
     *
     * @throws \ReflectionException
     */
    public function denormalize(mixed $data, string $type, ?string $format = null, array $context = []): mixed
    {
        if (!class_exists($type)) {
            throw new \InvalidArgumentException();
        }

        $reflectionMethod = new \ReflectionMethod($type, 'fromInteger');

        /** @var Identifier $identifier */
        $identifier = $reflectionMethod->invoke(null, $data);

        return $identifier;
    }

    /**
     * @param string $type
     * @param array<mixed> $context
     */
    public function supportsDenormalization(mixed $data, $type, ?string $format = null, array $context = []): bool
    {
        if ('[]' === substr($type, -2, 2)) {
            return false;
        }

        $interfaces = class_implements($type);
        Assert::isArray($interfaces);

        return in_array(IntWrapper::class, $interfaces);
    }

    /**
     * @param array<mixed>|false $context
     */
    public function normalize(mixed $object, ?string $format = null, array|false $context = []): int
    {
        Assert::isInstanceOf($object, IntWrapper::class);

        return $object->toInteger();
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed  $data   Data to normalize
     * @param string $format The format being (de-)serialized from or into
     * @param array<mixed> $context
     */
    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return $data instanceof IntWrapper;
    }

    /**
     * @return array<class-string, bool>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [IntWrapper::class => true];
    }
}
