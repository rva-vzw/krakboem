<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Infrastructure\EventStore;

use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

/**
 * A repository to store and retrieve events.
 *
 * You'll have to provide an implementation yourself.
 * (I might provide one in a KrakBoemBundle at some point in the future.)
 */
interface StoredEventRepository
{
    public function save(StoredEvent $storedEvent): void;

    /**
     * @return \Traversable<StoredEvent>
     */
    public function getAllForAggregate(
        AggregateRootIdentifier $aggregateRootIdentifier,
    ): \Traversable;

    public function hasByAggregateRoot(AggregateRootIdentifier $identifier): bool;

    /**
     * Returns all events; this is used for replay.
     *
     * @return \Traversable<StoredEvent>
     */
    public function getStream(): \Traversable;
}
