<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Infrastructure\EventStore\EventName;

use RvaVzw\KrakBoem\Event\Event;

interface EventNameResolver
{
    public function getNameByEvent(Event $event): string;

    /**
     * @return class-string<Event>
     */
    public function getEventClassByName(string $name): string;
}
