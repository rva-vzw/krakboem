<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Infrastructure\EventStore\EventName;

use RvaVzw\KrakBoem\Event\Event;
use Webmozart\Assert\Assert;

final class SimpleEventNameResolver implements EventNameResolver
{
    public function getNameByEvent(Event $event): string
    {
        return $event::class;
    }

    /**
     * @return class-string<Event>
     */
    public function getEventClassByName(string $name): string
    {
        Assert::isInstanceOf($name, Event::class);

        return $name;
    }
}
