<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Infrastructure\EventStore;

use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

interface EventStoreHacks
{
    public function deleteStreamForAggregate(AggregateRootIdentifier $aggregateRootIdentifier): void;
}
