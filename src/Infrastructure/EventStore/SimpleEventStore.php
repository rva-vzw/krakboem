<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Infrastructure\EventStore;

use RvaVzw\KrakBoem\Event\Event;
use RvaVzw\KrakBoem\EventStore\EventStore;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;
use RvaVzw\KrakBoem\Infrastructure\EventStore\EventName\EventNameResolver;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final readonly class SimpleEventStore implements EventStore
{
    public function __construct(
        private StoredEventRepository $eventRepository,
        private NormalizerInterface $normalizer,
        private DenormalizerInterface $denormalizer,
        private EventNameResolver $eventNameResolver,
    ) {
    }

    public function store(Event $event, int $playhead): void
    {
        $payload = $this->normalizer->normalize($event);

        if (!is_array($payload)) {
            throw new \InvalidArgumentException('Events should be normalized to arrays.');
        }

        $storedEvent = new StoredEvent(
            $event->getAggregateRootIdentifier(),
            $playhead,
            $this->eventNameResolver->getNameByEvent($event),
            $payload,
            new \DateTimeImmutable()
        );

        $this->eventRepository->save($storedEvent);
    }

    /**
     * @return \iterator<Event>
     */
    public function getStreamForAggregate(AggregateRootIdentifier $aggregateRootIdentifier): \iterator
    {
        foreach ($this->eventRepository->getAllForAggregate($aggregateRootIdentifier) as $storedEvent) {
            yield $storedEvent->getAggregateVersion() => $this->storedEventToEvent($storedEvent);
        }
    }

    public function hasStreamForAggregate(AggregateRootIdentifier $identifier): bool
    {
        return $this->eventRepository->hasByAggregateRoot(
            $identifier
        );
    }

    /**
     * Returns all events; this is used for replay.
     *
     * @return \iterator<Event>
     */
    public function getStream(): \iterator
    {
        foreach ($this->eventRepository->getStream() as $storedEvent) {
            yield $storedEvent->getAggregateVersion() => $this->storedEventToEvent($storedEvent);
        }
    }

    private function storedEventToEvent(StoredEvent $storedEvent): Event
    {
        $event = $this->denormalizer->denormalize(
            $storedEvent->getPayload(),
            $this->eventNameResolver->getEventClassByName($storedEvent->getEventType()),
        );
        Assert::isInstanceOf($event, Event::class);

        return $event;
    }
}
