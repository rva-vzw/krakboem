<?php

namespace RvaVzw\KrakBoem\Infrastructure\EventStore;

use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

/**
 * Event with metadata, to be stored in the event store.
 */
final readonly class StoredEvent
{
    private AggregateRootIdentifier $aggregateRootIdentifier;

    private int $aggregateVersion;

    private string $eventType;

    /**
     * @var array<mixed>
     */
    private array $payload;

    private \DateTimeImmutable $createdAt;

    /**
     * @param array<mixed> $payload
     */
    public function __construct(
        AggregateRootIdentifier $aggregateRootIdentifier,
        int $aggregateVersion,
        string $eventType,
        array $payload,
        \DateTimeImmutable $createdAt,
    ) {
        $this->aggregateRootIdentifier = $aggregateRootIdentifier;
        $this->aggregateVersion = $aggregateVersion;
        $this->eventType = $eventType;
        $this->payload = $payload;

        $this->createdAt = $createdAt;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->aggregateRootIdentifier;
    }

    public function getAggregateVersion(): int
    {
        return $this->aggregateVersion;
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    /**
     * @return array<mixed>
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
