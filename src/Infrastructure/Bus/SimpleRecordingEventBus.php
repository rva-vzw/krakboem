<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Infrastructure\Bus;

use RvaVzw\KrakBoem\Event\Event;
use RvaVzw\KrakBoem\EventBus\EventBus;
use RvaVzw\KrakBoem\EventListener\EventListener;
use RvaVzw\KrakBoem\EventStore\EventStore;

final readonly class SimpleRecordingEventBus implements EventBus
{
    /**
     * @param iterable<EventListener> $eventListeners
     */
    public function __construct(
        private EventStore $eventStore,
        private iterable $eventListeners,
    ) {
    }

    public function publish(Event $event, int $playhead): void
    {
        $this->eventStore->store($event, $playhead);

        foreach ($this->eventListeners as $listener) {
            $listener($event);
        }
    }
}
