<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Infrastructure\Bus;

use RvaVzw\KrakBoem\Event\Event;
use RvaVzw\KrakBoem\EventListener\Replayable;
use RvaVzw\KrakBoem\EventSourcing\Replay\ReplayBus;
use Webmozart\Assert\Assert;

final readonly class SimpleReplayBus implements ReplayBus
{
    /**
     * @param iterable<Replayable> $eventListeners
     */
    public function __construct(
        private iterable $eventListeners,
    ) {
    }

    public function publish(Event $event): void
    {
        foreach ($this->eventListeners as $listener) {
            Assert::isInstanceOf($listener, Replayable::class);
            $listener($event);
        }
    }
}
