<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Infrastructure\Bus;

use RvaVzw\KrakBoem\Command\Command;
use RvaVzw\KrakBoem\CommandBus\CommandBus;
use RvaVzw\KrakBoem\CommandHandler\CommandHandler;

final readonly class SimpleCommandBus implements CommandBus
{
    /**
     * @param iterable<CommandHandler> $commandHandlers
     */
    public function __construct(
        private iterable $commandHandlers,
    ) {
    }

    public function dispatch(Command ...$commands): void
    {
        foreach ($commands as $command) {
            foreach ($this->commandHandlers as $handler) {
                $handler($command);
            }
        }
    }
}
