<?php

namespace RvaVzw\KrakBoem\EventBus;

use RvaVzw\KrakBoem\Event\Event;

interface EventBus
{
    /**
     * Publishes the event that leads to the given version of the aggregate.
     */
    public function publish(Event $event, int $playhead): void;
}
