# KrakBoem!

This package contains interfaces and some classes I use for the
event sourced software I write in php.

I created it to learn about event sourcing. If you want to use
it, you can (under the conditions of the
[GNU Affero General Public License](LICENSE)), but as it turns out,
my opinion on how to do event sourcing in PHP is always changing,
so this is not a stable package. (I had the intention of making
it stable by releasing version 1.0, but I failed.)

For the moment, I have three projects that use krakboem:

* [dikdikdik](https://gitlab.com/rva-vzw/dikdikdik), my famous app to
  keep track of the score of the 
  [solo whist](https://en.wikipedia.org/wiki/Solo_whist) card game.
* [wdebelek](https://gitlab.com/rva-vzw/wdebelek), a web app to play
  card games. We used it in Covid times.
* [1jg](https://gitlab.com/johanv/1jg), another score app.

These apps all use the 1.0.1 version of the package, but I am currently
completely changing it for version 2.0. dikdikdik will probably migrate
to version 2.0.

## Expect these changes for version 2:

Almost everything changes:

* New namespaces
* I will provide basic implementations for the various buses, that
  don't depend on Symfony Messenger. (Because I never really used 
  the advanced features of Symfony Messenger.) You should be able
  to use Symfony Messenger in your own implementation of the various
  buses though.
* No more support for the old Aggregate class and the old
  EventSourcedWriteModelRepository. I will use deciders from now on.
* Use the normalizer of Symfony 7 (I want to decouple from Symfony
  normalizer as well, but probably not in version 2 of krakboem).
* ~Use cool new php 8.4 features~ (I added property hooks in #41,
  but I removed them again, because I like to keep the
  Commands and Events `readonly`.)

## How to install the package:

```
composer require rva-vzw/krakboem
```

## Running the tests with XDebug

If you are developing on this library, it can be useful to run the
tests with XDebug. I provided a docker-compose.yml file that should
make it rather easy to do with Linux and PhpStorm.

### Build and run the php container:

```
docker compose up -d
```

### Configure a server in PhpStorm

* Settings, PHP, Servers
* Add a server
  * Name: krakboem-xdebug
  * Host: irrelevant; this is not a webapp. You can type 'localhost'
  * Use path mappings: Map your project's root to /opt/app

I thought this would be enough for it to work when clicking
'Start Listening for PHP Debug Connections', and running
the tests as

```sh
docker compose exec php sh -c "make test"
```

So:

### Using a runtime configuration

- Go to the Run/Debug configurations in PHPStorm:

  > "Run" > "Edit configurations..."

- Add a new configuration:

  - Select "PHP Remote Debug"
  - Name: Xdebug on Docker
  - Filter debug connection by IDE key: false

- Now select "Xdebug on Docker" in the debug toolbar thingy, and
  click the icon on the right ('Run "Xdebug on docker"').

Run your tests within docker, e.g. like this:

```sh
docker compose exec php sh -c "make test"
```

