# CHANGELOG

# [2.0] - Unreleased

## Added

- Simple implementations for `CommandBus`, `EventBus` and `ReplayBus`, #43
- `EventNameResolver`, #36
- `Projector`, `ProcessManager`, `EventListenerTrait`, #45

## Changed

- php 8.4, #38
- namespaces and dependencies, #39
- ~property hooks in Command and Event, #41~ (reverted in #47)
- use `#[After]` attribute for event listeners, #42
- replaced `qossmic/deptrac` by `deptrac/deptrac`, #44
- command handler only handles applicable commands, #46
- nicer testing API, #35
- use serializer of Symfony 7, #48

## Removed

- Aggregate class, #40
- `AbstractProjector`, `AbstractProcessManager`, #45

# [1.0.1] - 2024-08-20

## Added

- Implemented `NormalizerInterface::getSupportedTypes()`, #37

# [1.0] - 2024-01-11

## Added

- deciders, #28
- EventStoreHack interface, #31
- Helper classes for testing, #33

## Changed

- php 8.3, #18
- Use 'Infrastructure' namespace for infrastructure, #27
- Return streams as Traversable instead of Generator, #30

## Removed

- removed dependency on Symfony, #29

## [0.11] - 2022-03-15

- php 8 and symfony 6, #17

## [0.10] - 2022-02-09

- Use ramsey/uuid 4.2, #16

## [0.9.1] - 2021-05-16

### Changed

- Required php version should be >= 7.4 instead of ^7.4

## [0.9] - 2021-05-16

### Changed

- Fix issue when using async event bus, #14

## [0.8] - 2020-09-22

### Changed

- Don't abort replay when an event listener throws an exception, #12

## [0.7] - 2020-09-19

### Changed

- Don't rely on Symfony 5.0, #11.

## [0.6] - 2020-04-15

### Changed

- The replay command shouldn't touch the database, #9

## [0.5] - 2020-04-16

### Changed

- Refactored write model repository, #5

## [0.4] - 2020-04-08

### Changed

- PHPDoc annotations for generics, #3

## [0.3] - 2020-04-03

### Changed

- depend on symfony 5, #2

## [0.2] - 2020-04-03

### Removed

- dependency on symfony/orm-pack, #1
- OrmStoredEventRepository and OrmEventStore, #1

## [0.1] - 2020-03-31

* Initial release
