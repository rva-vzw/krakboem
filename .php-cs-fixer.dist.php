<?php

//$finder = PhpCsFixer\Finder::create()
//    ->in(__DIR__)
//    ->exclude('vendor');

return (new PhpCsFixer\Config())->setRules([
    '@Symfony' => true,
    'array_syntax' => ['syntax' => 'short'],
    'phpdoc_align' => false,
]);
