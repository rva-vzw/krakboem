# Upgrade instructions for version 1.0

If you used a pre-1.0 version of krakboem, you will probably want to install
[rva-vzw/krakboem-bundle](https://gitlab.com/rva-vzw/krakboem-bundle/) as well.

First upgrade your app to Symfony 6.4, and rva-vzw/krakboem 0.11.1.

Then:

```bash
composer require rva-vzw/krakboem:^1.0 rva-vzw/krakboem-bundle
```

You'll get some error messages. Ignore them for now, and take care of the following:

* Make your event listeners that extend from `AbstractEventListener` readonly.
  If your projectors have non typed properties, you should fix that.
* Add the method `getAggregateRootIdentifier` to your commands, that 
  implement `RvaVzw\KrakBoem\Cqrs\CommandBus\Command`.
* `RvaVzw\KrakBoem\Infrastructure\IntWrapper` was moved to `RvaVzw\KrakBoem\Id\IntWrapper`.
* `RvaVzw\KrakBoem\Infrastructure\Identifier` was moved to `RvaVzw\KrakBoem\Id\Identifier`. The
   `fromString()` static constructor now expects a non-empty string, so you may have to take
   care of that.
* `RvaVzw\KrakBoem\Infrastructure\Uuid4Identifier` was moved to `RvaVzw\KrakBoem\Id\Uuid4Identifier`.
* `RvaVzw\KrakBoem\Infrastructure\Uuid5Identifier` was moved to `RvaVzw\KrakBoem\Id\Uuid5Identifier`.
* Make your identifiers that inherit from `Uuid4Identifier` or `Uuid5Identifier` readonly.
* `RvaVzw\KrakBoem\EventSourcing\Aggregate\AggregateRootIdentifier` was moved to `RvaVzw\KrakBoem\Id\AggregateRootIdentifier`
* `RvaVzw\KrakBoem\EventSourcing\EventBus\EventStoreMiddleware` is now 
  `RvaVzw\KrakBoemBundle\Messenger\EventStoreMiddleware`. You'll probably have to update that in your
  `messenger.yaml` configuration file.

If you use write model repositories (as opposed to deciders), you need to take into account of the following
as well:
* `RvaVzw\KrakBoem\EventSourcing\EventSourcedWriteModelRepository` was moved to
  `RvaVzw\KrakBoem\Cqrs\Aggregate\EventSourcedWriteModelRepository`, and became readonly. If you
  use these kind of write model repositories, make them readonly as well.
* `RvaVzw\KrakBoem\EventSourcing\Aggregate\AbstractAggregate` is now
  `RvaVzw\KrakBoem\Cqrs\Aggregate\AbstractAggregate`.
* `RvaVzw\KrakBoem\EventSourcing\Aggregate\Aggregate` is now
  `RvaVzw\KrakBoem\Cqrs\Aggregate\Aggregate`.

Remove the following service declarations from your services.yaml (because those services are now taken care of
in the bundle):

* `RvaVzw\KrakBoem\EventSourcing\Replay\Replayer` 
* `RvaVzw\KrakBoem\EventSourcing\EventBus\MessengerEventBus`
* `RvaVzw\KrakBoem\EventSourcing\Replay\ReplayConsoleCommand`
* `RvaVzw\KrakBoem\EventSourcing\EventBus\EventBus`
* `RvaVzw\KrakBoem\EventSourcing\EventBus\EventStoreMiddleware`
* `RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore`
* `RvaVzw\KrakBoem\EventSourcing\EventStore\SimpleEventStore`
* `RvaVzw\KrakBoem\Cqrs\CommandBus\MessengerCommandBus`
* `RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus`

If you didn't do something exotic, you can remove your local implementation of `StoredEventRepository`,
and use the default one from krakboem-bundle instead. But in that case you need to configure
an entity manager for the event store, you might want to refer to
[the krakboem-bundle documentation](https://gitlab.com/rva-vzw/krakboem-bundle/#doctrine)


If you need a service to delete stored events, e.g. to initialize your integration tests,
you can use `RvaVzw\KrakBoem\EventSourcing\EventStore\EventStoreHacks`

