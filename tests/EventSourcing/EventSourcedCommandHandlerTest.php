<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\EventSourcing;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\Command\Command;
use RvaVzw\KrakBoem\Decider\AggregateInternalState;
use RvaVzw\KrakBoem\Decider\Decider;
use RvaVzw\KrakBoem\Event\Event;
use RvaVzw\KrakBoem\EventBus\EventBus;
use RvaVzw\KrakBoem\EventSourcing\EventSourcedCommandHandler;
use RvaVzw\KrakBoem\EventStore\EventStore;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Command\JoinPlayer;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\CardTableIdentifier;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;

final class EventSourcedCommandHandlerTest extends TestCase
{
    #[Test]
    public function itPassesCorrectPlayheadToEventBus(): void
    {
        // Note to self: It's OK to use actual value objects from
        // RvaVzw\KrakBoem\Test\TestDomain.

        $command = new JoinPlayer(
            CardTableIdentifier::fromString('ed8e6ed7-c4ae-4190-b9cb-04d24f8c61ec'),
            PlayerName::fromString('Penn'),
        );

        $dummyEvent = $this->createMock(Event::class);

        $eventStoreMock = $this->createMock(EventStore::class);
        $eventStoreMock->method('getStreamForAggregate')->willReturnCallback(
            function () use ($dummyEvent) {yield $dummyEvent; },
        );

        $deciderMock = $this->createMock(Decider::class);
        $deciderMock->method('getInitialState')
            ->willReturn($this->createMock(AggregateInternalState::class));
        $deciderMock->method('evolve')
            ->willReturnCallback(
                fn (AggregateInternalState $internalState, Event $event) => $internalState,
            );
        $deciderMock->method('decide')->willReturnCallBack(
            function () use ($dummyEvent) {yield $dummyEvent; },
        );

        $eventBusMock = $this->createMock(EventBus::class);
        $eventBusMock->expects($this->once())
            ->method('publish')
            // expect playhead 2, because there was aready a 1st event in the stream.
            ->with($dummyEvent, 2);

        $handler = new readonly class($eventStoreMock, $eventBusMock, $deciderMock) extends EventSourcedCommandHandler {
            protected function acceptsCommand(Command $command): bool
            {
                // This handler accepts all commands. But in practice, you only want to accept the commands
                // that are relevant for your aggregate.
                return true;
            }
        };

        $handler($command);
    }

    #[Test]
    public function itIgnoresCommandsThatDoNotApply(): void
    {
        // Note to self: It's OK to use actual value objects from
        // the example domain.

        $command = new JoinPlayer(
            CardTableIdentifier::fromString('ed8e6ed7-c4ae-4190-b9cb-04d24f8c61ec'),
            PlayerName::fromString('Penn'),
        );

        $eventStoreMock = $this->createMock(EventStore::class);
        $eventStoreMock->expects($this->never())->method('getStreamForAggregate');

        $deciderMock = $this->createMock(Decider::class);
        $deciderMock->expects($this->never())->method('getInitialState');
        $deciderMock->expects($this->never())->method('decide');

        $eventBusMock = $this->createMock(EventBus::class);
        $eventBusMock->expects($this->never())
            ->method('publish');

        $handler = new readonly class($eventStoreMock, $eventBusMock, $deciderMock) extends EventSourcedCommandHandler {
            protected function acceptsCommand(Command $command): bool
            {
                // This handler doesn't accept commands
                return false;
            }
        };

        $handler($command);
    }
}
