<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\EventSourcing;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Event\PlayerJoined;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\CardTableIdentifier;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\ReadModel\PlayersGroup\PlayersGroup;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\ReadModel\PlayersGroup\PlayersGroupProjector;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\ReadModel\PlayersGroup\PlayersGroupRepository;

final class ProjectorTest extends TestCase
{
    #[Test]
    public function itListensToAnEvent(): void
    {
        $cardTableIdentifier = CardTableIdentifier::fromString('840a4ae8-525d-4705-892e-21e1ba9cfa55');
        $playerName = PlayerName::fromString('secr');

        $repositoryMock = $this->createMock(PlayersGroupRepository::class);
        $repositoryMock->method('getPlayersGroup')
            ->willReturn(PlayersGroup::createForTable($cardTableIdentifier));
        $repositoryMock->expects($this->once())
            ->method('savePlayersGroup')
            ->with(PlayersGroup::createForTable($cardTableIdentifier)->with($playerName));

        $projector = new PlayersGroupProjector($repositoryMock);
        $projector->__invoke(new PlayerJoined($cardTableIdentifier, $playerName));
    }
}
