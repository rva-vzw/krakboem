<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Exception;

use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;

final class DuplicatePlayer extends \Exception
{
    public static function byName(PlayerName $name): self
    {
        return new self("Duplicate player: {$name}");
    }
}
