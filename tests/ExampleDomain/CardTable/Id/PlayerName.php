<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id;

use RvaVzw\KrakBoem\Id\Identifier;

/**
 * A name of a player.
 *
 * For these examples, a player's name will identify the player
 * in the context of a Card Table.
 */
final readonly class PlayerName implements Identifier
{
    /**
     * @param non-empty-string $name
     */
    private function __construct(
        public string $name,
    ) {
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    public function toString(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
