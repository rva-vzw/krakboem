<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id;

use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;
use RvaVzw\KrakBoem\Infrastructure\Uuid\Uuid4Identifier;

final readonly class CardTableIdentifier extends Uuid4Identifier implements AggregateRootIdentifier
{
}
