<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Command;

use RvaVzw\KrakBoem\Command\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\CardTableIdentifier;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;

final readonly class JoinPlayer implements Command
{
    public function __construct(
        public CardTableIdentifier $cardTableIdentifier,
        public PlayerName $playerName,
    ) {
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->cardTableIdentifier;
    }
}
