<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\ExampleDomain\CardTable;

use RvaVzw\KrakBoem\Decider\AggregateInternalState;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;

final readonly class CardTable implements AggregateInternalState
{
    /**
     * @param array<int, PlayerName> $presentPlayers
     */
    private function __construct(
        public array $presentPlayers,
    ) {
    }

    public static function empty(): self
    {
        return new self([]);
    }

    public function withPlayer(PlayerName $playerName): self
    {
        if ($this->hasPlayer($playerName)) {
            return $this;
        }

        return new self(array_merge(
            $this->presentPlayers,
            [$playerName],
        ));
    }

    public function hasPlayer(PlayerName $playerName): bool
    {
        return in_array($playerName, $this->presentPlayers);
    }
}
