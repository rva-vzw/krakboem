<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Decider;

use RvaVzw\KrakBoem\Decider\AbstractDecider;
use RvaVzw\KrakBoem\Decider\AggregateInternalState;
use RvaVzw\KrakBoem\Decider\Applies;
use RvaVzw\KrakBoem\Decider\Handles;
use RvaVzw\KrakBoem\Event\Event;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\CardTable;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Command\JoinPlayer;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Event\PlayerJoined;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Exception\DuplicatePlayer;

/**
 * @extends AbstractDecider<CardTable>
 */
final readonly class CardTableDecider extends AbstractDecider
{
    public function getInitialState(): AggregateInternalState
    {
        return CardTable::empty();
    }

    public function isTerminal(AggregateInternalState $internalState): bool
    {
        return false;
    }

    /**
     * @return \Traversable<Event>
     */
    #[Handles(JoinPlayer::class)]
    public function handleJoinPlayer(
        CardTable $cardTable,
        JoinPlayer $command,
    ): \Traversable {
        if ($cardTable->hasPlayer($command->playerName)) {
            throw DuplicatePlayer::byName($command->playerName);
        }

        yield new PlayerJoined(
            $command->cardTableIdentifier,
            $command->playerName,
        );
    }

    #[Applies(PlayerJoined::class)]
    public function afterPlayerJoined(
        CardTable $cardTable,
        PlayerJoined $event,
    ): CardTable {
        return $cardTable->withPlayer($event->playerName);
    }
}
