<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\ReadModel\PlayersGroup;

use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\CardTableIdentifier;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;

final readonly class PlayersGroup
{
    /**
     * @param list<PlayerName> $playerNames
     */
    private function __construct(
        public CardTableIdentifier $cardTableIdentifier,
        public array $playerNames,
    ) {
    }

    public static function createForTable(CardTableIdentifier $cardTableIdentifier): self
    {
        return new self($cardTableIdentifier, []);
    }

    public function with(PlayerName $playerName): self
    {
        $newNames = array_merge($this->playerNames, [$playerName]);

        return new self($this->cardTableIdentifier, $newNames);
    }
}
