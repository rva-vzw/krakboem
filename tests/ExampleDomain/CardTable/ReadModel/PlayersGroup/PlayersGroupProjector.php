<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\ReadModel\PlayersGroup;

use RvaVzw\KrakBoem\EventListener\After;
use RvaVzw\KrakBoem\EventListener\EventListenerTrait;
use RvaVzw\KrakBoem\EventSourcing\Projector;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Event\PlayerJoined;

final readonly class PlayersGroupProjector implements Projector
{
    use EventListenerTrait;

    public function __construct(
        private PlayersGroupRepository $playersGroupRepository,
    ) {
    }

    #[After(PlayerJoined::class)]
    public function afterPlayerJoined(PlayerJoined $playerJoined): void
    {
        $group = $this->playersGroupRepository->getPlayersGroup(
            $playerJoined->cardTableIdentifier,
        );

        $this->playersGroupRepository->savePlayersGroup(
            $group->with($playerJoined->playerName),
        );
    }
}
