<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\ReadModel\PlayersGroup;

use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\CardTableIdentifier;

interface PlayersGroupRepository
{
    public function getPlayersGroup(CardTableIdentifier $cardTableIdentifier): PlayersGroup;

    public function savePlayersGroup(PlayersGroup $playersGroup): void;
}
