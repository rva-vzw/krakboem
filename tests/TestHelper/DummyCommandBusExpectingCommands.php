<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\TestHelper;

use PHPUnit\Framework\Assert;
use RvaVzw\KrakBoem\Command\Command;
use RvaVzw\KrakBoem\CommandBus\CommandBus;

final class DummyCommandBusExpectingCommands implements CommandBus
{
    /** @var Command[] */
    private $expectedCommands;

    /**
     * DummyCommandBusExpectingCommands constructor.
     *
     * @param Command[] $expectedCommands
     */
    public function __construct(array $expectedCommands)
    {
        $this->expectedCommands = $expectedCommands;
    }

    public function dispatch(Command ...$commands): void
    {
        foreach ($commands as $command) {
            $this->shift($command);
        }
    }

    private function shift(Command $command): void
    {
        $expectedCommand = array_shift($this->expectedCommands);
        Assert::assertEquals($expectedCommand, $command);
    }
}
