<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\TestHelper;

use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\Decider\AggregateInternalState;
use RvaVzw\KrakBoem\Decider\Decider;
use RvaVzw\KrakBoem\Event\Event;

/**
 * @template TInternalState of AggregateInternalState
 */
abstract class DeciderTestCase extends TestCase
{
    /**
     * @return Decider<TInternalState>
     */
    abstract protected function getDecider(): Decider;

    /**
     * @param class-string<\Throwable> $throwable
     */
    public function setExpectedThrowable(string $throwable): void
    {
        $this->expectException($throwable);
    }

    /**
     * @return Given<TInternalState>
     */
    public function given(Event ...$given): Given
    {
        return new Given(
            $this,
            $this->getDecider(),
            array_values($given),
        );
    }
}
