<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\TestHelper;

use PHPUnit\Framework\Assert;
use RvaVzw\KrakBoem\Command\Command;
use RvaVzw\KrakBoem\Decider\AggregateInternalState;
use RvaVzw\KrakBoem\Decider\Decider;
use RvaVzw\KrakBoem\Event\Event;

/**
 * @template TInternalState of AggregateInternalState
 */
final readonly class When
{
    /**
     * @param DeciderTestCase<TInternalState> $deciderTestCase
     * @param Decider<TInternalState> $decider
     * @param list<Event> $given
     */
    public function __construct(
        private DeciderTestCase $deciderTestCase,
        private Decider $decider,
        private array $given,
        private Command $when,
    ) {
    }

    public function then(Event ...$then): void
    {
        $stateBeforeTest = array_reduce(
            $this->given,
            fn ($state, Event $event) => $this->decider->evolve($state, $event),
            $this->decider->getInitialState(),
        );

        Assert::assertEquals(
            $then,
            iterator_to_array($this->decider->decide(
                $stateBeforeTest,
                $this->when,
            )),
        );
    }

    /**
     * @param class-string<\Throwable> $expectedThrowable
     */
    public function expectException(string $expectedThrowable): void
    {
        $stateBeforeTest = array_reduce(
            $this->given,
            fn ($state, Event $event) => $this->decider->evolve($state, $event),
            $this->decider->getInitialState(),
        );

        $this->deciderTestCase->setExpectedThrowable($expectedThrowable);

        // Iterate through the generated events, otherwise the exception
        // may pass unnoticed
        iterator_to_array($this->decider->decide(
            $stateBeforeTest,
            $this->when,
        ));
    }
}
