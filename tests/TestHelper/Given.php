<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\TestHelper;

use RvaVzw\KrakBoem\Command\Command;
use RvaVzw\KrakBoem\Decider\AggregateInternalState;
use RvaVzw\KrakBoem\Decider\Decider;
use RvaVzw\KrakBoem\Event\Event;

/**
 * @template TInternalState of AggregateInternalState
 */
final readonly class Given
{
    /**
     * @param DeciderTestCase<TInternalState> $deciderTestCase used to pass
     *                         expected exceptions back to phpunit
     * @param Decider<TInternalState> $decider
     * @param list<Event> $given
     */
    public function __construct(
        private DeciderTestCase $deciderTestCase,
        private Decider $decider,
        private array $given,
    ) {
    }

    /**
     * @return When<TInternalState>
     */
    public function when(Command $command): When
    {
        return new When(
            $this->deciderTestCase,
            $this->decider,
            $this->given,
            $command,
        );
    }
}
