<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\Decider;

use PHPUnit\Framework\Attributes\Test;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\CardTable;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Command\JoinPlayer;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Decider\CardTableDecider;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Event\PlayerJoined;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Exception\DuplicatePlayer;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\CardTableIdentifier;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;
use RvaVzw\KrakBoem\Test\TestHelper\DeciderTestCase;

/**
 * @extends DeciderTestCase<CardTable>
 */
final class ExampleDeciderTest extends DeciderTestCase
{
    private CardTableIdentifier $cardTableIdentifier;

    protected function setUp(): void
    {
        $this->cardTableIdentifier = CardTableIdentifier::fromString('9bb28686-f571-4dc8-919e-d6862fe108ca');
    }

    protected function getDecider(): CardTableDecider
    {
        return new CardTableDecider();
    }

    #[Test]
    public function itJoinsAPlayer(): void
    {
        $this->given()
                ->when(new JoinPlayer(
                    $this->cardTableIdentifier,
                    PlayerName::fromString('tlf'),
                ))->then(new PlayerJoined(
                    $this->cardTableIdentifier,
                    PlayerName::fromString('tlf'),
                ));
    }

    #[Test]
    public function itJoinsAnAdditionalPlayer(): void
    {
        $this->given(
            new PlayerJoined(
                $this->cardTableIdentifier,
                PlayerName::fromString('tlf'),
            ),
            new PlayerJoined(
                $this->cardTableIdentifier,
                PlayerName::fromString('dpb'),
            )
        )->when(new JoinPlayer(
            $this->cardTableIdentifier,
            PlayerName::fromString('jlf'),
        )
        )->then(new PlayerJoined(
            $this->cardTableIdentifier,
            PlayerName::fromString('jlf'),
        )
        );
    }

    #[Test]
    public function itDoesNotJoinTheSamePlayerTwice(): void
    {
        $this->given(
            new PlayerJoined(
                $this->cardTableIdentifier,
                PlayerName::fromString('tlf'),
            ))->when(new JoinPlayer(
                $this->cardTableIdentifier,
                PlayerName::fromString('tlf'),
            ))->expectException(DuplicatePlayer::class);
    }
}
