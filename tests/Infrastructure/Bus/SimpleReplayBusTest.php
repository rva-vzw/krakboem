<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\Infrastructure\Bus;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\EventListener\Replayable;
use RvaVzw\KrakBoem\Infrastructure\Bus\SimpleReplayBus;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Event\PlayerJoined;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\CardTableIdentifier;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;

final class SimpleReplayBusTest extends TestCase
{
    #[Test]
    public function itPublishesEvents(): void
    {
        $event = new PlayerJoined(
            CardTableIdentifier::fromString('b6ee730e-7262-4d4f-b640-6cbd386a7b24'),
            PlayerName::fromString('dtl'),
        );

        $listener = $this->createMock(Replayable::class);
        $listener->expects($this->once())
            ->method('__invoke')
            ->with($event);

        $bus = new SimpleReplayBus([$listener]);
        $bus->publish($event);
    }
}
