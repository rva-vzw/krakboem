<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\Infrastructure\Bus;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\EventListener\EventListener;
use RvaVzw\KrakBoem\EventStore\EventStore;
use RvaVzw\KrakBoem\Infrastructure\Bus\SimpleRecordingEventBus;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Event\PlayerJoined;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\CardTableIdentifier;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;

final class SimpleRecordingEventBusTest extends TestCase
{
    #[Test]
    public function itRecordsAndPublishesEvents(): void
    {
        $event = new PlayerJoined(
            CardTableIdentifier::fromString('b6ee730e-7262-4d4f-b640-6cbd386a7b24'),
            PlayerName::fromString('dtl'),
        );

        $store = $this->createMock(EventStore::class);
        $store->expects($this->once())
            ->method('store')
            ->with($event, 1);

        $listener = $this->createMock(EventListener::class);
        $listener->expects($this->once())
            ->method('__invoke')
            ->with($event);

        $bus = new SimpleRecordingEventBus($store, [$listener]);
        $bus->publish($event, 1);
    }
}
