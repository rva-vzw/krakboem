<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\Infrastructure\Bus;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\CommandHandler\CommandHandler;
use RvaVzw\KrakBoem\Infrastructure\Bus\SimpleCommandBus;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Command\JoinPlayer;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\CardTableIdentifier;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;

final class SimpleCommandBusTest extends TestCase
{
    #[Test]
    public function itDispatchesCommands(): void
    {
        $command = new JoinPlayer(
            CardTableIdentifier::fromString('eba5d834-c3b6-4e5f-b81b-5d5516984863'),
            PlayerName::fromString('dtl'),
        );

        $handler = $this->createMock(CommandHandler::class);
        $handler->expects($this->once())
            ->method('__invoke')
            ->with($command);

        $bus = new SimpleCommandBus([$handler]);
        $bus->dispatch($command);
    }
}
