<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Test\Infrastructure\EventStore;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\Infrastructure\EventStore\EventName\EventNameResolver;
use RvaVzw\KrakBoem\Infrastructure\EventStore\SimpleEventStore;
use RvaVzw\KrakBoem\Infrastructure\EventStore\StoredEvent;
use RvaVzw\KrakBoem\Infrastructure\EventStore\StoredEventRepository;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Event\PlayerJoined;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\CardTableIdentifier;
use RvaVzw\KrakBoem\Test\ExampleDomain\CardTable\Id\PlayerName;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class SimpleEventStoreTest extends TestCase
{
    #[Test]
    public function itStoresAnEvent(): void
    {
        $dummyDenormalizer = $this->createMock(DenormalizerInterface::class);
        $dummyNormalizer = $this->createMock(NormalizerInterface::class);
        // We don't care about the return value of the normalizer, so we can just return an empty array
        $dummyNormalizer->method('normalize')->willReturn([]);

        $eventNameResolverStub = $this->createMock(EventNameResolver::class);
        $eventNameResolverStub->method('getNameByEvent')->willReturn('test_event');

        $eventRepositoryMock = $this->createMock(StoredEventRepository::class);
        $eventRepositoryMock->expects($this->once())->method('save')
            ->with($this->callback(fn (StoredEvent $storedEvent) => 'test_event' === $storedEvent->getEventType()));

        $eventStore = new SimpleEventStore(
            $eventRepositoryMock,
            $dummyNormalizer,
            $dummyDenormalizer,
            $eventNameResolverStub,
        );

        $eventStore->store(
            new PlayerJoined(
                CardTableIdentifier::fromString('c1822ff4-768c-4327-aceb-77d202fc7c4c'),
                PlayerName::fromString('John Doe'),
            ),
            1
        );
    }

    #[Test]
    public function itDenormalizesEvents(): void
    {
        $payload = [
            'table' => 'c1822ff4-768c-4327-aceb-77d202fc7c4c',
            'player' => 'John Doe',
        ];

        $dummyNormalizer = $this->createMock(NormalizerInterface::class);

        $denormalizerStub = $this->createMock(DenormalizerInterface::class);
        $denormalizerStub->expects($this->once())
            ->method('denormalize')
            ->with($payload, PlayerJoined::class)
            ->willReturn(new PlayerJoined(
                CardTableIdentifier::fromString('c1822ff4-768c-4327-aceb-77d202fc7c4c'),
                PlayerName::fromString('John Doe'),
            ));

        $eventNameResolverStub = $this->createMock(EventNameResolver::class);
        $eventNameResolverStub->method('getEventClassByName')->willReturn(PlayerJoined::class);

        $eventRepositoryStub = $this->createMock(StoredEventRepository::class);
        $eventRepositoryStub->method('getAllForAggregate')->willReturn(new \ArrayIterator([
            new StoredEvent(
                CardTableIdentifier::fromString('c1822ff4-768c-4327-aceb-77d202fc7c4c'),
                1,
                'test_event',
                $payload,
                new \DateTimeImmutable(),
            ),
        ]));

        $eventStore = new SimpleEventStore(
            $eventRepositoryStub,
            $dummyNormalizer,
            $denormalizerStub,
            $eventNameResolverStub,
        );

        $events = iterator_to_array($eventStore->getStreamForAggregate(
            CardTableIdentifier::fromString('c1822ff4-768c-4327-aceb-77d202fc7c4c')
        ));

        $this->assertCount(1, $events);
        $this->assertInstanceOf(PlayerJoined::class, reset($events));
    }
}
