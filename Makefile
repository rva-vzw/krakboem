.PHONY: qa fix php-cs-fixer phpstan deptrac reset-read-model reset replay test

qa: deptrac php-cs-fixer phpstan

php-cs-fixer:
	PHP_CS_FIXER_IGNORE_ENV=1 ./vendor/bin/php-cs-fixer --dry-run fix .

phpstan:
	php -d memory_limit=4G ./vendor/bin/phpstan analyse --no-progress

deptrac:
	./vendor/bin/deptrac --no-progress

fix:
	PHP_CS_FIXER_IGNORE_ENV=1 ./vendor/bin/php-cs-fixer fix .

# Targets below were copied from dikdikdik;
# not sure yet if I 'll keep them.

reset-read-model:
	./bin/console doctrine:schema:drop --force
	./bin/console doctrine:schema:create

# Prevent duplicate deployment with a mkdir statement.

replay:
	mkdir replay_lock
	echo MAINTENANCE=true >> .env.local
	./bin/console cache:clear
	./bin/console eventsourcing:readmodel:replay
	sed -i '/^MAINTENANCE=true$$/d' .env.local
	./bin/console cache:clear
	rmdir replay_lock

reset: reset-read-model
	./bin/console doctrine:schema:drop --force --em=event_store
	./bin/console doctrine:schema:create --em=event_store

test:
	./vendor/bin/phpunit tests --testdox --colors=auto
